# QUILL-flow-cell

25 cm² flow cell for flow battery testing, originally reproduced from the following paper by O'Conner et al: https://doi.org/10.1039/D1SE01851E 

On the publisher's version of the paper, the following license is given: https://creativecommons.org/licenses/by/3.0/

